pipeline{
    agent{
        label 'vfd-agent'
    }
    environment{
        PROJECT_NAME = ""
        PORT = ""
        GIT_URL = ""
        SONAR_CREDENTIALS = ""
        BRANCH = "develop"
        SONAR_HOME = "${tool 'devops-sonar-scanner-4'}"
        NODEJS_HOME = "${tool 'devops-nodejs'}"
        PATH = "${env.SONAR_HOME}/bin:${env.NODEJS_HOME}/bin:${env.PATH}"
    }

    stages{
        /*
            Checkout source code
        */
        stage('Checkout') {
          steps {
              script{
                  checkout([$class: 'GitSCM', 
                    branches: [[name: "${env.BRANCH}"]],
                    doGenerateSubmoduleConfigurations: false, 
                    extensions: [], gitTool: 'jgitapache',
                    userRemoteConfigs: [[ url: "${env.GIT_URL}"]]
                    ])
                }
                
            }
        }
        stage('Install TypeScript'){
            steps{
                sh """
                    cp ./src/package*.json . && npm install --production --silent
                """
            }
        }
        stage("Sonarqube") {
            steps {
                script{
                    withSonarQubeEnv(credentialsId: "${env.SONAR_CREDENTIALS}", installationName: 'Devops-sonar') {
                        sh "sonar-scanner " +
                            "-Dsonar.projectKey=${env.PROJECT_NAME} " +
                            "-Dsonar.projectName=${env.PROJECT_NAME} " +
                            "-Dsonar.sourceEncoding=UTF-8 " +
                            "-Dsonar.branch.name=${env.BRANCH} "+                   
                            "-Dsonar.sources=. " +
                            "-Dsonar.projectVersion=${env.BUILD_ID}"
                    }
                }
            }
        }
        /*
            Check quality gate
        */
        stage("Quality Gate"){
            steps{
                script{
                    sleep 20
                    def qualityGate = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
                    if (qualityGate.status != 'OK') {
                        error 'Sonar quality gate fail.'
                    }
                }
            }
        }
        stage("Build and deploy app"){
            environment {
                TAG=sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                PUBLIC_IP= '13.65.95.249'
            }
            steps{
                sh "docker build -t sfd-nodejs:${env.TAG} ."
                sh "docker rm -f ${PROJECT_NAME} || true"
                sh "docker run -d -p ${env.PORT}:3000 --name ${env.PROJECT_NAME} --restart=always sfd-nodejs:${env.TAG}"
                echo "Access to http://${env.PUBLIC_IP}:${env.PORT}"
            }
        }
    }

}
